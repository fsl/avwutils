#!/bin/sh

#   basic image handling scripts
#
#   FMRIB Image Analysis Group
#
#   Copyright (C) 1999-2004 University of Oxford
#
#   SHCOPYRIGHT

/bin/rm epimj[1-6].* 

for fn in ANALYZE:1 NIFTI_PAIR:2 NIFTI:3 ANALYZE_GZ:4 NIFTI_PAIR_GZ:5 NIFTI_GZ:6 ; do
  num=`echo $fn | sed 's/.*://'`;
  opt=`echo $fn | sed 's/:.*//'`;
  export FSLOUTPUTTYPE=$opt
  echo FSLOUTPUTTYPE=$opt
  echo ./fslswapdim epi4 y z x epimj${num}
  ./fslswapdim epi4 y z x epimj${num}
  echo ./fslmaths epi4 -mul 0.001 episs${num} -odt float
  ./fslmaths epi4 -mul 0.001 episs${num} -odt float
done

for fn in epimj[1-6].* episs[1-6].* ; do
  val=`../niftiio/nifti1_test $fn | grep nifti_type`;
  echo "  XXXXX $fn XXXXX $val "
done

